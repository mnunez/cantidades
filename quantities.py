#!/usr/bin/env pythoh3

# MARTA NÚÑEZ GARCÍA

'''
Maneja un diccionario con cantidades
'''

import sys

items = {}

# Función "op_add": añade a la despensa (diccionario "items") el artículo con su correspondiente cantidad. Si el
# artículo ya está en la despensa, se sustituye la cantidad que había por la actual.
def op_add():
    """Add an item and quantity from argv to the items dictionary"""

    try:
        item = str(sys.argv[0])
        quantity = int(sys.argv[1])
    except:
        ValueError("La cantidad debe ser un número entero.")

    new_item = True

    items[item] = quantity
    for key, value in items.items():
        if key == item:
            new_item = False
    if not new_item:
        items.pop(key)
        items[item] = quantity


# Función "op_items": muestra la lista de artículos en la despensa. Para ello se crea una lista vacía (items_list) en
# la cual se van añadiendo los artículos y, posteriormente, se separan mediante espacios.
def op_items():
    """Print all items, separated by spaces"""

    items_list = []
    for key, value in items.items():
        items_list.append(key)
    item_str = " ".join(items_list)
    print("Items: " + item_str)


# Función "op_all": muestra todos los artículos que hay en la despensa con sus correspondientes cantidades. Para ello
# se crea una lista vacía (all_list) en la cual se van añadiendo los datos (artículos y cantidades) y, posteriormente,
# se separan mediante espacios. Las cantidades se muestran entre paréntesis.
def op_all():
    """Print all items and quantities, separated by spaces"""

    all_list = []
    for key, value in items.items():
        all_list.append(key + " " + "(" + str(value) + ")")
    all_str = " ".join(all_list)
    print("All: " + all_str)


# Función "op_sum": muestra la suma (total) de lo que hay en la despensa (suma total de cantidades).
def op_sum():
    """Print sum of all quantities"""

    suma = 0
    for key, value in items.items():
        suma = suma + value
    print("Sum: " + str(suma))


# Función "main": mientras se introduzca una lista de argumentos, extrae el primer argumento de la lista (parámetro 0).
# Si corresponde a alguna de las palabras asociadas a las funciones, llama a la función correspondiente.
def main():

    while sys.argv:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "all":
            op_all()
        elif op == "sum":
            op_sum()


if __name__ == '__main__':
    main()